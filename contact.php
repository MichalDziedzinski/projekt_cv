<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  $myObj = new \stdClass();//bez zastosowania tego

  $validationFlag = true;

  //name validation
  $name = trim($_POST['name']);
  $name = htmlentities($name, ENT_QUOTES, "UTF-8");
  
  if ($name == ''){
    $myObj->alert_name = 'Podaj imię i nazwisko lub nazwę firmy';
    $validationFlag = false;
  }

  //phone validation
  $phone=trim($_POST['phone']);
	$phone=preg_replace('/\D+/','',$phone);//do czyszczenia znaków niebędących cyframi
	$phone=preg_replace('/\s+/', '',$phone);//do czyszczenia białych znaków
	$phone=preg_replace('/^48/','',$phone);//czyszczenie prefixu polskiego
	$phone=preg_replace('/^0/','',$phone);
	$phone=preg_replace('/^0/','',$phone);
	$phone=preg_replace('/^48/','',$phone);

  //mail validation
  $mail = trim($_POST['mail']);

  if ($mail == ''){
    $myObj->alert_email_null = 'Podaj adres e-mail';
    $validationFlag = false;
  }

	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL)){
    $myObj->alert_email_incorrect = 'Podaj poprawny adres e-mail';
    $validationFlag = false;
  }
  
  //message validation
  $message=trim($_POST['message']);
  $message = htmlentities($message, ENT_QUOTES, "UTF-8");
  if ($message == ''){
    $myObj->alert_message = 'Napisz wiadomość';
    $validationFlag = false;
  }

  //Captcha validation
  $captcha = $_POST['captcha'];
  if ($captcha!=''){
    $secret_key = '6LehE0QUAAAAAMRpYykm9npJ8F1TEf1vgAK9_V0q';
    $captcha_check=file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$captcha);
    $response = json_decode($captcha_check);
  
    if (!($response->success)){
      $myObj->alert_captcha = 'Potwierdź, że nie jesteś robotem';
      $validationFlag = false;
    }
  }
  else{
    $myObj->alert_captcha = 'Potwierdź, że nie jesteś robotem';
    $validationFlag = false;
  }
  //mail
  if ($validationFlag){
    $to = 'michal.marcin.dziedzinski@gmail.com';

    $subject = 'Wiadomość ze strony dziedzinski.pl';

    $header  = "MIME-Version: 1.0\r\n";
    $header .= "Content-type: text/html; charset=UTF-8\r\n";
    $header .= "From: ".$name." <".$email.">\r\n";
    $header .= "Reply-To: ".$email."\r\n";
    $header .= "Return-Path: michal.marcin.dziedzinski@gmail.com\r\n";

    $msg = "<html>
    <body>
        <b>Wiadomość ze strony dziedzinski.pl</b><br>
        Telefon: {$phone} <br>
        E-mail: {$mail} <br>
        {$name}  napisał:<br>
        <pre 'style=\"font-size:16px;\"'>{$message}</pre>
    </body>
    </html>";
    $mailSend = mail($to, $subject, $msg, $header);

    $to = $mail;
    $msg = "<html>
    <body>
        <b>Kopia wiadomości ze strony dziedzinski.pl</b><br>
        Telefon: {$phone} <br>
        E-mail: {$mail} <br>
        {$name}  napisał:<br>
        <pre 'style=\"font-size:16px;\"'>{$message}</pre>
    </body>
    </html>";
    
    $mailSendCopy = mail($to, $subject, $msg, $header);
  }

  $myObj->validationFlag = $validationFlag;

  $myJSON = json_encode($myObj);
  
  echo $myJSON;
}

?>