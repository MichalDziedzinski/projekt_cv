const gulp = require('gulp'); 
const browserSync = require('browser-sync').create(); 
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const changed = require('gulp-changed');
const htmlReplace = require('gulp-html-replace');
const htmlMin = require('gulp-htmlmin');
const del = require('del');
const sequence = require('run-sequence');

var config = {
  dist: 'dist/',
  src: './',
  cssin: 'assets/css/**/*.css',
  jsin: 'assets/js/**/*.js',
  imgin: 'assets/img/**/*.{jpg,jpeg,png,gif}',
  htmlin: '*.html',
  scssin: 'assets/sass/**/*.scss',
  cssout: 'dist/assets/css/',
  jsout: 'dist/assets/js',
  imgout: 'dist/assets/img',
  htmlout: 'dist/',
  scssout: 'css/',
  cssoutname: 'style.css',
  jsoutname: 'script.js',
  cssreplaceout: 'assets/css/style.css',
  jsreplaceout: 'assets/js/script.js'
};

gulp.task('serve', ['sass'], function() {
  browserSync.init({
    server: config.src
  }); 
  gulp.watch(config.htmlin).on('change', browserSync.reload);
  gulp.watch(config.cssin).on('change', browserSync.reload);
  gulp.watch(config.scssin, ['sass']);
  gulp.watch(config.jsin).on('change', browserSync.reload);
});

gulp.task('sass', function() {
  return gulp.src(config.scssin)
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 3 versions']
  }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(config.scssout))
  .pipe(browserSync.stream());
})

gulp.task('css', function() {
  return gulp.src(config.cssin)
  .pipe(concat(config.cssoutname))
  .pipe(cleanCSS())
  .pipe(gulp.dest(config.cssout));
});

gulp.task('js', function() {
  return gulp.src(config.jsin)
  .pipe(concat(config.jsoutname))
  .pipe(uglify())
  .pipe(gulp.dest(config.jsout));
});

gulp.task('img', function(){
  return gulp.src(config.imgin)
  .pipe(changed(config.imgout))
  .pipe(imagemin())
  .pipe(gulp.dest(config.imgout));
})

gulp.task('html', function() {
  return gulp.src(config.htmlin)
  .pipe(htmlReplace({
    'css': config.cssreplaceout,
    'js': config.jsreplaceout
  }))
  .pipe(htmlMin({
    sortAttributes: true,
    sortClassName: true,
    collapseWhitespace: true
  }))
  .pipe(gulp.dest(config.dist))
})

gulp.task('clean', function() {
return del([config.dist]);
});

gulp.task('build', function(){
  sequence('clean', ['html', 'js', 'css', 'img']);
});

gulp.task('default', ['serve']);
