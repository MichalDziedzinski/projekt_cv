var photo = document.querySelector('.photo');
var skill = document.querySelectorAll('.skill');
var imgFlag = false;
var skillsNames = ['HTML', 'CSS', 'Bootstrap', 'JavaScript', 'jQuery', 'PHP', 'CodeIgniter', 'WordPress', 'MySQL']

photo.addEventListener('click', function () {
	if (imgFlag == false) {
		photo.setAttribute('src', 'assets/img/photo_alt.jpg');
		imgFlag = true;
		return false;
	}
	photo.setAttribute('src', 'assets/img/photo.jpg');
	imgFlag = false;
});

for (var i = 0; i < skill.length; i++) //odtworzenie animacji skill barów które aktualnie wyświetlają się na stronie
{
	if (isInViewport(skill[i])) {
		skill[i].classList.add("skill--" + skillsNames[i]);
	}
}

i = 0;

window.addEventListener('scroll', function () { //odtworzenie animacji skill barów w momencie kiedy dany scroll bar jest widoczny
	if (i == skill.length) {
		return false;
	}
	if (isInViewport(skill[i])) {
		skill[i].classList.add("skill--" + skillsNames[i]);
		i++;
	}
});

function isInViewport(element) {
	var rect = element.getBoundingClientRect(); //Metoda Element.getBoundingClientRect() zwraca rozmiar  oraz  położenie elementu względem okna widoku (viewport).
	var html = document.documentElement; //Zwraca Element będący bezpośrednim dzieckiem document
	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <= (window.innerHeight || html.clientHeight) &&
		rect.right <= (window.innerWidth || html.clientWidth) // client dla IE
	);
}

var form = document.querySelector('.form');
//inputy
var nameInput = document.querySelector('.form-input--name');
var phoneInput = document.querySelector('.form-input--phone');
var emailInput = document.querySelector('.form-input--email');
var messageInput = document.querySelector('.form-input--message');
//alerty
var alertName = document.querySelector('.alert--name');
var alertPhone = document.querySelector('.alert--phone');
var alertEmail = document.querySelector('.alert--email');
var alertMessage = document.querySelector('.alert--message');
var alertCaptcha = document.querySelector('.alert--captcha');
//treść po wysłaniu
var thanks = document.querySelector('.thanks');
var rewriteButton = document.querySelector('.rewrite-button');

form.addEventListener('submit', function (e) {

	e.preventDefault();

	//wartości inputów
	var name = nameInput.value.trim();
	var phone = phoneInput.value.trim();
	var email = emailInput.value.trim();
	var message = messageInput.value.trim();
	var captcha = grecaptcha.getResponse();

	var formFlag = true; //zmienna której stan zależy od poprawności danych w formularzu

	formFlag = nameChecking(name, alertName);
	formFlag = phoneChecking(phone, alertPhone);
	formFlag = emailChecking(email, alertEmail);
	formFlag = messageChecking(message, alertMessage);
	formFlag = captchaChecking(captcha, alertCaptcha);

	if (!formFlag) {
		e.preventDefault();
		return false;
	}

	var data = "name=" + name + "&phone=" + phone + "&mail=" + email + "&message=" + message + "&captcha=" + captcha;

	var request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			alertName.textContent = myObj.alert_name;
			alertEmail.textContent = myObj.alert_email_null;
			if(myObj.alert_email_null==0 || myObj.alert_email_null==null){
				alertEmail.textContent = myObj.alert_email_incorrect;
			}
			alertMessage.textContent = myObj.alert_message;
			alertCaptcha.textContent = myObj.alert_captcha;
			var validationFlag = myObj.validationFlag;
			if (validationFlag){
				form.style.display="none";
				thanks.style.display="flex";
			}
		}
	}
	request.open('POST', 'contact.php', true);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	request.send(data);


});

rewriteButton.addEventListener('click', function () {

	form.style.display="block";
	thanks.style.display="none";
	grecaptcha.reset();
	alertCaptcha.textContent='';
});

nameInput.addEventListener('blur', function () {

	var name = nameInput.value.trim();

	formFlag = nameChecking(name, alertName);
	if (formFlag)
	{
		nameInput.classList.remove('input--red');
		nameInput.classList.add('input--green');
		return false;
	}
	nameInput.classList.remove('input--green');
	nameInput.classList.add('input--red');

});

phoneInput.addEventListener('blur', function () {

	var phone = phoneInput.value.trim();

	formFlag = phoneChecking(phone, alertPhone);

	if(phone=='' || phone==null){
		return false;
	}

	if (formFlag)
	{
		phoneInput.classList.remove('input--red');
		phoneInput.classList.add('input--green');
		return false;
	}
	phoneInput.classList.remove('input--green');
	phoneInput.classList.add('input--red');

});

emailInput.addEventListener('blur', function () {

	var email = emailInput.value.trim();

	formFlag = emailChecking(email, alertEmail);

	if (formFlag)
	{
		emailInput.classList.remove('input--red');
		emailInput.classList.add('input--green');
		return false;
	}
	emailInput.classList.remove('input--green');
	emailInput.classList.add('input--red');
});

messageInput.addEventListener('blur', function () {

	var message = messageInput.value.trim();
	formFlag = messageChecking(message, alertMessage);

	if (formFlag)
	{
		messageInput.classList.remove('input--red');
		messageInput.classList.add('input--green');
		return false;
	}
	messageInput.classList.remove('input--green');
	messageInput.classList.add('input--red');
});

nameInput.addEventListener('focus', function () {
	nameInput.classList.remove('input--red');
	nameInput.classList.remove('input--green');
	alertName.textContent='';
});

phoneInput.addEventListener('focus', function () {
	phoneInput.classList.remove('input--red');
	phoneInput.classList.remove('input--green');
	alertPhone.textContent='';

});

emailInput.addEventListener('focus', function () {
	emailInput.classList.remove('input--red');
	emailInput.classList.remove('input--green');
	alertEmail.textContent='';
});

messageInput.addEventListener('focus', function () {
	messageInput.classList.remove('input--red');
	messageInput.classList.remove('input--green');
	alertMessage.textContent='';
});


function nameChecking(name, alertName) {

	if (name == '' || name == null) {
		alertName.textContent = 'Podaj imię i nazwisko lub nazwę firmy';
		return false;
	}
	alertName.textContent = '';
	return true;
}

function phoneChecking(phone, alertPhone) {
	// phone = phone.replace(/^\D+/g, '');
	phone = phone.replace(new RegExp('^48', ''));
	phone = phone.replace(new RegExp('^0'), '');
	phone = phone.replace(new RegExp('^0'), '');
	phone = phone.replace(new RegExp('^48', ''));
	var regExpTel = /^\d+$/;
	if (!regExpTel.test(phone) && phone != '' && phone != null) {
		alertPhone.textContent = 'Podaj poprawny numer telefonu';
		phone = phone.replace(/\s+/g, '');
		return false;
	}
	alertPhone.textContent = '';
	return true;
}

function emailChecking(email, alertEmail) {
	var regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i;

	if (email == '' || email == null) {
		alertEmail.textContent = 'Podaj adres e-mail';
		return false;
	}
	if (!regExpEmail.test(email)) {
		alertEmail.textContent = 'Podaj poprawny adres e-mail';
		return false;
	}
	alertEmail.textContent = '';
	return true;
}

function messageChecking(message, alertMessage) {

	if (message == '' || message == null) {
		alertMessage.textContent = 'Napisz wiadomość';
		return false;
	}
	alertMessage.textContent = '';
	return true;
}

function captchaChecking(captcha, alertCaptcha) {

	if (captcha == '' || captcha == null) {
		alertCaptcha.textContent = 'Potwierdź, że nie jesteś robotem';
		return false;
	}
	alertCaptcha.textContent = '';
	return true;
}

//na zaliczenie z js

// //container
// var container = document.querySelector('.container--js');
// //first
// var mainPhoto = document.querySelector('.main-photo--js');
// var target = document.querySelector('.target--js');
// var skills = document.querySelector('.skills--js');
// var direction = document.querySelector('.direction--js');
// var language = document.querySelector('.language--js');
// var hobby = document.querySelector('.hobby--js');
// //second
// var experience = document.querySelector('.experience--js');
// var education = document.querySelector('.education--js');
// //contact
// var contact = document.querySelector('.contact--js');

// //adding content

// //header

// document.querySelector('.header-first--js').textContent = 'Curriculum vitae';

// //first

// //main-photo

// mainPhoto.querySelector('.photo').setAttribute('src', 'assets/img/photo.jpg');

// //target

// target.querySelector('.header-second-first-col').textContent = 'Cel zawodowy';

// target.querySelector('.para').textContent = 'Jestem początkującym Web Developerem. Moim celem jest wykonywanie pracy pozwalającej na udział w ciekawych i rozwijąjących moje umiejętności projektach.';

// //skills
// skills.querySelector('.header-second-first-col').textContent = 'Umiejętności';
// var skillsName = ['HTML', 'CSS', 'Bootstrap', 'JavaScript', 'jQuery', 'PHP','CodeIgniter','WordPress','MySQL', 'GIT']
// var skillsLevel = [60,50,30,40,50,20,20,40,50,60];
// var table = document.createElement('table');
// var tbody = document.createElement('tbody');
// var length = skillsName.length;

// skills.appendChild(table);
// table.appendChild(tbody);

// for (var r = 0, length = skillsName.length; r < length; r++) {
//     var myRow = document.createElement('tr'); //tworzenie wierszy

//     for (var c = 0; c < 2; c++) {
//         var myCol = document.createElement('td'); //tworzenie kolumn

//         var textCell;
//         var textNode;

//         if (c == 0) {
//             textNode = '<img class="skillBarImg" src="assets/img/'+skillsName[r]+'.png" alt="'+skillsName[r]+'">';
//             myRow.appendChild(myCol);
//             myCol.innerHTML = textNode;
//         } else {
//             textNode = '<div class="skillBar"><div class="skill skill'+skillsName[r]+'"></div><p class="d-flex align-items-center">'+skillsName[r]+'</p></div>';
//             myRow.appendChild(myCol);
//             myCol.innerHTML = textNode;
//         }
//     }
//     table.appendChild(myRow);
// }
// //second

// //experience
// experience.querySelector('.header-second-second-col').textContent="Doświadczenie";
// experience.querySelector('.para-experience').textContent="10/2017-obecnie";
// experience.querySelector('.header-third-second-col').textContent="Junior Web Developer";
// experience.querySelector('.companyImg').setAttribute('src', 'assets/img/Adsira.png');
// experience.querySelector('.header-fourth-second-col').textContent="Zakres obowiązków:";

// var tasks=['Tworzenie stron internetowych na podstawie projektu graficznego','Tworzenie CRUD do bloga w PHP za pomocą frameworka CodeIgniter', 'Edycja wyglądu templatek WordPress zgodnie z wytycznymi klienta','Tworzenie podstron forów stworzonych w phpBB','Tworzenie skryptów walidujących dane wprowadzone przez użytkowników'];

// var list=document.createElement('ul');
// experience.appendChild(list);
// for (var i = 0, length = tasks.length; i < length; i++) {
//     var listElements=document.createElement('li');
//     list.appendChild(listElements);
//     listElements.textContent=tasks[i];
// }
// //education
// education.querySelector('h2')[0].textContent="Wykształcenie";
// education.querySelector('p')[0].textContent="10/2017-06/2018";
// education.querySelector('h3')[0].textContent="Politechnika Białostocka";
// education.querySelector('img')[0].setAttribute('src', 'assets/img/pb.png');
// education.querySelector('h4')[0].textContent="Zakres obowiązków:";
// education.querySelector('p')[1].textContent="10/2017-06/2018";
// education.querySelector('h3')[1].textContent="Wojskowa Akademia Techniczna";
// education.querySelector('img')[1].setAttribute('src', 'assets/img/Wat.png');
// education.querySelector('h4')[1].textContent="Zakres obowiązków:";
// education.querySelector('p')[2].textContent="10/2017-06/2018";
// education.querySelector('h3')[2].textContent="Politechnika Wrocławska";
// education.querySelector('img')[2].setAttribute('src', 'assets/img/pwr.png');
// education.querySelector('h4')[2].textContent="Zakres obowiązków:";